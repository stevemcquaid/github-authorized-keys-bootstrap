USERNAMEINPUT=deploy
HOST="j1900.local"

rsync -Pah --exclude='.git/' --checksum $PWD $USERNAMEINPUT@$HOST:~/

ssh $USERNAMEINPUT@$HOST 'sudo /bin/bash -x ~/github-authorized-keys-bootstrap/remote-install.sh'

sleep 5 # wait 5 seconds for unit to startup
ssh stevemcquaid@$HOST