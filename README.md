# Github-Authorized-Keys Bootstrapper
These scripts install Github-Authorized-Keys on a remote system.

# Usage:
To use, modify the variables on line 1 & 2 of `install.sh`

Then run: 

  * `cd github-authorized-keys-bootstrapper`
  * `./install.sh`

# Issues
  * `ssh.service` - For the first run, ssh.service is unable to be restarted.
    * You will manually need to perform this action. `remote-install.sh` does do this.
    * You will also find the following error in the logs: 
      * `{"job":"sshIntegrate","level":"info","msg":"Output: sh: 0: getcwd() failed: No such file or directory\nFailed to connect to bus: No data available\nFailed to connect to bus: No data available\n","subsystem":"jobs","time":"2018-10-12T18:58:55Z"}`
      * `{"job":"sshIntegrate","level":"error","msg":"Error: exit status 1","subsystem":"jobs","time":"2018-10-12T18:58:55Z"}`


