cp ~/github-authorized-keys-bootstrap/etc/github-authorized-keys /etc/github-authorized-keys
cp ~/github-authorized-keys-bootstrap/gak.service /etc/systemd/system/github-authorized-keys.service

systemctl daemon-reload
systemctl restart github-authorized-keys.service
systemctl enable github-authorized-keys.service

systemctl restart ssh
systemctl restart github-authorized-keys.service